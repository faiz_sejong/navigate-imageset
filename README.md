# Navigate ImageSet

This repository contains the source code for Simple GUI application for navigating in the ImageSet based on camera positions (extrinsic parameters). The repo is indended to be used to the Exploration study of JPEG GMIS. 
This Repository also contain a Matlab Application (.mlapp) file and a directory named "example_data". The directory "example_data" contains two directories "model_as_text" and "orig_images". Using the COLMAP software for the "orig_images", camera intrinsic and extrinsic parameters are obtained and exported to "model_as_text".  The application uses "example_data\models_as_text\cameras.txt" and "example_data\models_as_text\images.txt" for navigating in the "example_data\orig_images"


The GUI  Application is developed in Matlab 2023. 

